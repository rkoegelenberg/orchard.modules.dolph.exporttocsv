﻿(function () {
    var fieldsContainer = $('#fields-container'),
        contentTypes = $('#contenttypes'),
        chooseFields = $('#choose-fields'),
        emptyFields = $('#empty-fields');

    fieldsContainer.on('change', '#select-all', function () {
        if ($(this).is(':checked')) {
            chooseFields.find('input').prop('checked', true);
        }
        else {
            chooseFields.find('input').removeAttr('checked');
        }

    });

    contentTypes.on('change', function (e) {
        if ($(this).val() != '') {
            $.post('/Admin/ExportToCsv/GetFields', $(this).parents('form').first().serialize(), function (data) {
                if (data.length) {
                    var listItems = '<label for="select-all" style="font-weight:bold;"><input type="checkbox" id="select-all" checked="true" />Select/Unselect All</label>';
                    $.each(data, function (index, item) {
                        listItems += '<input type="checkbox" name="fields" value="' + item + '"  checked="true" />' + item + '<br/>';
                    });
                    chooseFields.html(listItems);
                    fieldsContainer.slideDown();
                    emptyFields.hide();
                }
                else {
                    chooseFields.html('');
                    fieldsContainer.slideUp();
                    emptyFields.show();
                }
            });
        }
        else {
        }
    });
})();