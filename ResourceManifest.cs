﻿using Orchard.UI.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dolph.ExportToCsv
{
    public class ResourceManifest : IResourceManifestProvider
    {
        public void BuildManifests(ResourceManifestBuilder builder)
        {
            var manifest = builder.Add();
            manifest.DefineScript("ExportToCsv")
                .SetUrl("ExportToCsv.js")
                .SetDependencies("jQuery");

        }
    }
}