﻿using Dolph.ExportToCsv.Services;
using Dolph.ExportToCsv.ViewModels;
using Orchard;
using Orchard.ContentManagement;
using Orchard.ContentManagement.MetaData;
using Orchard.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Orchard.UI.Notify;

namespace Dolph.ExportToCsv.Controllers
{
    public class AdminController : Controller
    {
        private readonly IContentDefinitionManager _contentDefinitionManager;
        private readonly IExportService _exportService;
        private readonly IContentManager _contentManager;

        public AdminController(IOrchardServices services, 
                               IContentManager contentManager, 
                               IContentDefinitionManager contentDefinitionManager,
                               IExportService exportService )
        {
            _contentDefinitionManager = contentDefinitionManager;
            _exportService = exportService;
            _contentManager = contentManager;
            Services = services;
        }

        public IOrchardServices Services { get; private set; }
        public Localizer T { get; set; }

        public ActionResult Export()
        {
            var viewModel = new ExportViewModel();
            viewModel.ContentTypes = new List<string>();
            foreach (var contentType in _contentDefinitionManager.ListTypeDefinitions().OrderBy(c => c.Name))
            {
                viewModel.ContentTypes.Add(contentType.Name);
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult GetFields(string contentTypes)
        {
            var fields = _exportService.GetFields(contentTypes);
            return Json(fields);
        }

        [HttpPost, ActionName("Export")]
        public ActionResult ExportPOST()
        {
            if (!Services.Authorizer.Authorize(Permissions.Export, T("Not allowed to export.")))
                return new HttpUnauthorizedResult();

            var viewModel = new ExportViewModel
            {
                ContentTypes = new List<string>()
            };

            foreach (var contentType in _contentDefinitionManager.ListTypeDefinitions().OrderBy(c => c.Name))
            {
                viewModel.ContentTypes.Add(contentType.Name);
            }

            var selectedContentType = Request.Form["contentTypes"];
            var fields = Request.Form["fields"] == null ? new string[0] : Request.Form["fields"].Split(new string[]{","}, StringSplitOptions.RemoveEmptyEntries);
            if (_contentManager.Query(selectedContentType).Count() == 0)
            {
                Services.Notifier.Error(T(string.Format("No content items found for the {0} content type.", selectedContentType)));                
                ModelState.AddModelError("contentTypes", T(string.Format("No content items found for the {0} content type.", selectedContentType)).Text);
            }
            else if (fields.Length == 0)
            {
                Services.Notifier.Error(T(string.Format("Please choose at least one field to export for the {0} content type.", selectedContentType)));
                ModelState.AddModelError("fields", T(string.Format("Please choose at least one field to export for the {0} content type.", selectedContentType)).Text);
            }
            else
            {
                var result = _exportService.Export(selectedContentType, fields);

                ModelState.Clear();
                return File(new System.Text.UTF8Encoding().GetBytes(result), "text/csv", "export.csv");
            }
            return View(viewModel);
        }
    }
}