﻿using Orchard;
using Orchard.ContentManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Text.RegularExpressions;

namespace Dolph.ExportToCsv.Services
{
    public interface IExportService : IDependency
    {
        string Export(string contentTypes, string[] fields);
        IEnumerable<string> GetFields(string contentType);
    }

    public class ExportService : IExportService
    {
        private readonly IOrchardServices _orchardServices;
        private readonly IContentManager _contentManager;
        public ExportService(IContentManager contentManager,
                             IOrchardServices orchardServices)
        {
            _contentManager = contentManager;
            _orchardServices = orchardServices;
        }

        public string Export(string contentType, string[] fields)
        {
            var list = _contentManager.Query(contentType).List();
            var headerStringBuilder = new StringBuilder();
            var detailStringBuilder = new StringBuilder();
            var headerRowGenerated = false;
            foreach (var item in list)
            {
                var element = _contentManager.Export(item);
                foreach (var attribute in element.Attributes())
                {
                    if (Array.IndexOf(fields, attribute.Name.ToString()) > - 1)
                    {
                        if (!headerRowGenerated)
                        {
                            headerStringBuilder.Append(attribute.Name + ",");
                        }
                        detailStringBuilder.Append(attribute.Value + ",");
                    }
                }
                foreach (var descendant in element.Descendants())
                {
                    foreach (var attribute in descendant.Attributes())
                    {
                        if (Array.IndexOf(fields, descendant.Name + "-" + attribute.Name.ToString()) > -1)
                        {

                            if (!headerRowGenerated)
                            {
                                headerStringBuilder.Append(descendant.Name + "-" + attribute.Name + ",");
                            }
                            detailStringBuilder.Append(attribute.Value + ",");
                        }
                    }                    
                }
                headerRowGenerated = true;
                detailStringBuilder.Append(Environment.NewLine);
            }

            return headerStringBuilder.ToString() + Environment.NewLine + detailStringBuilder.ToString();
        }

        public IEnumerable<string> GetFields(string contentType)
        {
            var fields = new List<string>();
            var firstItem = _contentManager.Query(contentType).List().FirstOrDefault();
            if (firstItem != null)
            {
                var element = _contentManager.Export(firstItem);
                foreach (var item in element.Attributes())
                {
                    fields.Add(item.Name.ToString());
                }
                foreach (var descendant in element.Descendants())
                {
                    foreach (var attribute in descendant.Attributes())
                    {
                        fields.Add(descendant.Name + "-" + attribute.Name.ToString());
                    }
                }
            }
            return fields;
        }

    }

}